# Ian's Cool Website

This is the repo for my own personal website. I'ts built on top of React (via `create-react-app`), Typescript, SASS, and React Router. It's designed with React's hook features in mind.

## Installation

Like most React apps using `react-scripts` 

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode with hot reload.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. Lint errors will be displayed in the console.

### `npm test`

Launches the Jest test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It bundles React in production mode and optimizes the build.\ The code is minified and the filenames include the hashes.
