import React from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import HomePage from './pages/HomePage';
import TestPage from './pages/TestPage';
import ErrorPage from './pages/ErrorPage';
import './styles/app.scss';

function App()  : React.ReactElement
{
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<HomePage />} />
        {developmentRoutes()}
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </BrowserRouter>
  );
}

function developmentRoutes()  : React.ReactElement | null
{
  if (process.env.NODE_ENV === 'development')
  {
    return (
      <Route path='development'>
        <Route path='components' element={<TestPage />}/>
        <Route index element={<ErrorPage />} />
      </Route>
    );
  }
  else
    return null;
}

export default App;
