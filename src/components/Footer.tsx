import React from 'react';

import '../styles/components/Footer.scss';

function Footer() : React.ReactElement
{
    return (
        <footer className='footer'>
            Made with ♥ by Ian Donovan. &copy; {new Date().getFullYear()}
        </footer>
    );
}

export default Footer;