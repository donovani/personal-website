import React from 'react';

import '../styles/components/TitleBar.scss';

function TitleBar() : React.ReactElement
{
    return (
        <div className='title-bar'>
            <h1>IBBITZ</h1>
            <span className='text-caption'>The works of Ian Donovan</span>
        </div>
    );
}

export default TitleBar;