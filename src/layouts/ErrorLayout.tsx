import React, { PropsWithChildren } from 'react';
import TitleBar from '../components/TitleBar';
import Footer from '../components/Footer';
import '../styles/layouts/ErrorLayout.scss';

function ErrorLayout(props : PropsWithChildren<{}>) : React.ReactElement<PropsWithChildren<{}>>
{
    return (
        <div className='layout-vertical error-layout'>
            <TitleBar />
            <div className='layout-content'>
                {props.children}
            </div>
            <Footer />
        </div>
    );
}

export default ErrorLayout;