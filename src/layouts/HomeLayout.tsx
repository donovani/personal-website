import React, { PropsWithChildren } from 'react';
import TitleBar from '../components/TitleBar';
import Footer from '../components/Footer';
import '../styles/layouts/HomeLayout.scss';

function HomeLayout(props : PropsWithChildren<{}>) : React.ReactElement<PropsWithChildren<{}>>
{
    return (
        <div className='layout-vertical page-layout'>
            <TitleBar />
            <div className='layout-content'>
                {props.children}
            </div>
            <Footer />
        </div>
    );
}

export default HomeLayout;