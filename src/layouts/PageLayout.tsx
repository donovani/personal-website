import React, { PropsWithChildren } from 'react';
import TitleBar from '../components/TitleBar';
import Footer from '../components/Footer';
import '../styles/layouts/PageLayout.scss';

type PageLayoutProps = {
    headerColor: string;
};

function PageLayout(props : PropsWithChildren<PageLayoutProps>) : React.ReactElement<PropsWithChildren<PageLayoutProps>>
{
    return (
        <div className='layout-vertical page-layout'>
            <TitleBar />
            <div className='layout-content'>
                {props.children}
            </div>
            <Footer />
        </div>
    );
}

export default PageLayout;