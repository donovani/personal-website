import React, { PropsWithChildren } from 'react';
import TitleBar from '../components/TitleBar';
import Footer from '../components/Footer';

import '../styles/layouts/ProjectLayout.scss';

function ProjectLayout(props : PropsWithChildren<{}>) : React.ReactElement<PropsWithChildren<{}>>
{
    return (
        <div className='project-layout'>
            <TitleBar />
            <div className='layout-content'>
                {props.children}
            </div>
            <Footer />
        </div>
    );
}

export default ProjectLayout;