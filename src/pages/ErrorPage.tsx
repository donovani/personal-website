import React from 'react';
import ErrorLayout from '../layouts/ErrorLayout';

function ErrorPage() : React.ReactElement
{
    return (
        <ErrorLayout>
            404 Page goes here!
        </ErrorLayout>
    );
}

export default ErrorPage;