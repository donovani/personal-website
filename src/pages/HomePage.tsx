import React from 'react';
import HomeLayout from '../layouts/HomeLayout';

function HomePage() : React.ReactElement
{
    return (
        <HomeLayout>
            Home Page goes here!
        </HomeLayout>
    );
}

export default HomePage;