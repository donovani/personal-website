import React from 'react';

import '../styles/text.scss';
import '../styles/buttons.scss';
import PageLayout from '../layouts/PageLayout';

function TestPage() : React.ReactElement
{
    return (
        <PageLayout headerColor='transparent'>
            
            <h1>Header 1</h1>
            <h2>Header 2</h2>
            <h3>Header 3</h3>
            <h4>Header 4</h4>
            <h5>Header 5</h5>
            <h6>Header 6</h6>

            <span className='text-display'>Display</span> <br/>
            <span className='text-title'>Title</span> <br/>
            <span className='text-header'>Header</span> <br/>
            <span className='text-subheader'>Subheader</span> <br/>
            <span className='text-body'>Body</span> <br/>
            <span className='text-caption'>Caption</span> <br/>
            <span className='text-small'>Fineprint</span> <br/>

            <a href='#'>Link</a> <br/>

            <hr/>

            <button>Button</button>
        </PageLayout>
    );
}


export default TestPage;